const { ObjectID } = require('mongodb');

const { mongoose } = require('../server/db/mongoose');
const { Todo } = require('../server/models/todo');
const { User } = require('../server/models/user');

// Todo.remove({}).then((result) => {
//   console.log(result);
// });

// Todo.findOneAndRemove();
// Todo.findByIdAndRemove();

// Todo.findByIdAndRemove('5953bb37e90a9cfd81130f47').then((removedTodo) => {
//   console.log(JSON.stringify(removedTodo, undefined, 2));
// });

Todo.findOneAndRemove({_id: '5953bba4e90a9cfd81130f5e'}).then((removedTodo) => {
  console.log(JSON.stringify(removedTodo, undefined, 2));
});
