const { ObjectID } = require('mongodb');

const { mongoose } = require('../server/db/mongoose');
const { Todo } = require('../server/models/todo');
const { User } = require('../server/models/user');

// const ID = '59528e464f0d6818d4e8c5831';

// if (!ObjectID.isValid(ID)) {
//   console.log('ID not valid');
// }

// Todo.find({
//   _id: ID
// }).then((todos) => {
//   console.log('Todos', todos);
// });
//
// Todo.findOne({
//   _id: ID
// }).then((todo) => {
//   if (!todo) {
//     return console.log('ID not found');
//   }
//
//   console.log('Todo', todo);
// });

// Todo.findById(ID).then((todo) => {
//   if (!todo) {
//     return console.log('ID not found');
//   }
//
//   console.log('Todo by ID', todo);
// }).catch((e) => console.log(e));

const USER_ID = '595164209f614c250cba1f10';

User.findById(USER_ID).then((user) => {
  if (!user) {
    return console.log('User not found');
  }

  console.log('User', user);
}).catch((e) => console.log(e));
