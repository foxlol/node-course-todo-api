const { SHA256 } = require('crypto-js');
const bcrypt = require('bcryptjs');

const password = '123abc!';

bcrypt.genSalt(10, (err, salt) => {
  bcrypt.hash(password, salt, (err, hash) => {
    console.log('Hash', hash);
  });
});

const hashedPassword = '$2a$10$WIQyu5jVc1alxeYUrWLLTeY/BTlKRYpK9Oi.bDu9INmeioJCHAcy6';

bcrypt.compare(password, hashedPassword, (err, result) => {
  console.log('Result', result);
});

// const message = 'I am Uhtred, son of Uhtred!';
// const hash = SHA256(message).toString();
//
// console.log(`Message: ${message}`);
// console.log(`Hash: ${hash}`);
//
// const data = {
//   id: 4
// };
//
// const token = {
//   data,
//   hash: SHA256(JSON.stringify(data) + 'somesecret').toString()
// }

//Man in the middle
// token.data.id = 5;
// token.hash = SHA256(JSON.stringify(token.data)).toString();

// const resultHash = SHA256(JSON.stringify(token.data) + 'somesecret').toString();
//
// if (resultHash === token.hash) {
//   console.log('Data wasn\'t changed');
// } else {
//   console.log('Data was changed. Don\'t trust!');
// }
