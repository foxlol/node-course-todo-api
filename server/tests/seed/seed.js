const { ObjectID } = require('mongodb');
const jwt = require('jsonwebtoken');

const { Todo } = require('../../models/todo');
const { User } = require('../../models/user');

const userOneId = new ObjectID();
const userTwoId = new ObjectID();

const userOnetoken =
  jwt.sign(
    {
      _id: userOneId.toHexString(), access: 'auth'
    }, process.env.JWT_SECRET).toString();

const userTwotoken =
  jwt.sign(
    {
      _id: userTwoId.toHexString(), access: 'auth'
    }, process.env.JWT_SECRET).toString();

const USERS = [{
  _id: userOneId,
  email: 'foxlol@gmail.com',
  password: 'userOnePass',
  tokens: [{
    access: 'auth',
    token: userOnetoken
  }]
}, {
  _id: userTwoId,
  email: 'aaa@aaa.com',
  password: 'userTwoPass',
  tokens: [{
    access: 'auth',
    token: userTwotoken
  }]
}];

const TODOS = [{
  _id: new ObjectID(),
  text: 'Task 1',
  _creator: userOneId
}, {
  _id: new ObjectID(),
  text: 'Task 2',
  completed: true,
  completedAt: 123344,
  _creator: userTwoId
}];

const populateUsers = (done) => {
  User.remove({}).then(() => {
    const userOne = new User(USERS[0]).save();
    const userTwo = new User(USERS[1]).save();

    return Promise.all([userOne, userTwo]);
  }).then(() => done()).catch((e) => console.log(e));
};

const populateTodos = (done) => {
  Todo.remove({}).then(() => {
    return Todo.insertMany(TODOS);
  }).then(() => done()).catch((e) => console.log(e));
};

module.exports = {
  TODOS, USERS,
  populateTodos, populateUsers
};
